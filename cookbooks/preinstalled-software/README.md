# preinstalled-software

Installs common software that can be used to during CI execution.

The following software is being installed:

- [Chocolatey v0.10.15](https://chocolatey.org/)
- [Visual Studio 2019 Build Tools v16.2.3.0](https://chocolatey.org/packages/visualstudio2019buildtools)
  - [Chocolatey Visual Studio servicing extensions v1.8.1](https://chocolatey.org/packages/chocolatey-visualstudio.extension)
  - [KB2919355](https://chocolatey.org/packages/KB2919355)
  - [KB2919442](https://chocolatey.org/packages/KB2919442)
  - [KB2999226](https://chocolatey.org/packages/KB2999226)
  - [kb2919355](https://chocolatey.org/packages/kb2919355)
  - [Microsoft .NET Framework 4.8](https://chocolatey.org/packages/dotnetfx)
    - [KB2919355](https://chocolatey.org/packages/KB2919355)
- [Visual Studio Build Tools components](https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-build-tools?view=vs-2019)
  - [.Net desktop build tools](https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-build-tools?view=vs-2019#net-desktop-build-tools)
  - [.NET Core build tools](https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-build-tools?view=vs-2019#net-core-build-tools)
  - [C++ build tools](https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-build-tools?view=vs-2019#c-build-tools)
  - [Web development build tools](https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-build-tools?view=vs-2019#web-development-build-tools)
  - [Mobile Development with .NET](https://docs.microsoft.com/en-us/visualstudio/install/workload-component-id-vs-build-tools?view=vs-2019#mobile-development-with-net)
- [Chocolatey Windows Update extensions](https://chocolatey.org/packages/chocolatey-windowsupdate.extension)
- [Nuget v5.2.0](https://chocolatey.org/packages/NuGet.CommandLine/5.2.0)
- [7zip 19.0](https://chocolatey.org/packages/7zip)
- [curl v7.65.3](https://chocolatey.org/packages/curl)
  - `curl` is a built in PowerShell alias, so please use `curl.exe` or use `Remove-Item Alias:curl` in your CI job.
- [wget v1.20.3.20190531](https://chocolatey.org/packages/Wget)
  - `wget` is a built in PowerShell alias, so please use `wget.exe` or use `Remove-Item Alias:wget` in your CI job.
- [ruby v2.6.3.1](https://chocolatey.org/packages/ruby)
- [Go v1.13.0](https://chocolatey.org/packages/golang)
- [Nodejs v12.10.0](https://chocolatey.org/packages/nodejs)
- [OpenJDK v12.0.2](https://chocolatey.org/packages/openjdk)
- [jq v1.5](https://chocolatey.org/packages/jq)
- [docker](https://docs.microsoft.com/en-us/virtualization/windowscontainers/quick-start/set-up-environment?tabs=Windows-Server#install-docker)
  - This is being installed based on the `INSTALL_DOCKER_VERSION` environment variable, for base images that do not provide Docker pre-installed.
- [docker-compose](https://chocolatey.org/packages/docker-compose)
- [vcpkg 2019.12](https://github.com/microsoft/vcpkg)
