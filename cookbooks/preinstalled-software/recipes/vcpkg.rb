git 'vcpkg' do
    repository 'https://github.com/microsoft/vcpkg.git'
    checkout_branch '2019.12'
    destination 'C:\\vcpkg'
end

execute 'Install vcpkg' do
    command 'C:\\vcpkg\\bootstrap-vcpkg.bat'
end

execute 'Integrate vcpkg systemwide' do
    command 'C:\\vcpkg\\vcpkg integrate install'
end

windows_path 'C:\vcpkg' do
    action :add
end
